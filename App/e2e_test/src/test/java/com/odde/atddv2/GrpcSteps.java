package com.odde.atddv2;

import com.github.leeonky.jfactory.JFactory;
import com.odde.atddv2.grpc.GreeterGrpc;
import com.odde.atddv2.grpc.HelloReply;
import com.odde.atddv2.grpc.HelloRequest;
import io.cucumber.java.zh_cn.当;
import io.cucumber.java.zh_cn.那么;
import io.grpc.Channel;
import io.grpc.ManagedChannelBuilder;

import static com.github.leeonky.dal.Assertions.expect;

public class GrpcSteps {

    private HelloReply john;

    @当("call {string}:")
    public void call(String method, String request) {
        Channel channel = ManagedChannelBuilder.forAddress("localhost", 4770)
                .usePlaintext()
                .build();
        GreeterGrpc.GreeterBlockingStub greeterBlockingStub = GreeterGrpc.newBlockingStub(channel);
        JFactory jFactory = new JFactory();
        jFactory.factory(HelloRequest.Builder.class).constructor(inc -> HelloRequest.newBuilder());

        HelloRequest.Builder builder = jFactory.type(HelloRequest.Builder.class).property("name", "John").create();
//        john = greeterBlockingStub.sayHello(HelloRequest.newBuilder().setName("John").build());
        john = greeterBlockingStub.sayHello(builder.build());
    }

    @那么("grpc response be:")
    public void grpcResponseBe(String verification) {
        expect(john).should(verification);
    }
}
