package com.odde.atddv2.db;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.source.ConfigurationPropertySources;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Component
public class DatabaseConfig {

    @Autowired
    private Environment environment;

    @Autowired
    private JpaProperties jpaProperties;

    private DataSource dataSource(String config) {
        Binder binder = new Binder(ConfigurationPropertySources.get(environment));
        DataSourceProperties dataSourceProperties = binder.bind(config, DataSourceProperties.class)
                .orElse(new DataSourceProperties());
        return dataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }

    public EntityManagerFactory entityManagerFactory(String configPrefix, String... packagesToScan) {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(jpaProperties.isGenerateDdl());
        vendorAdapter.setShowSql(jpaProperties.isShowSql());
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource(configPrefix));
        em.setPackagesToScan(packagesToScan);
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaPropertyMap(jpaProperties.getProperties());
        em.afterPropertiesSet();
        return em.getObject();
    }
}
